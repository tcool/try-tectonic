## try-tectonic


## 事前準備

```bash
# install Rust
$ curl https://sh.rustup.rs -sSf | sh

# install tectonic
$ brew install tectonic

```

## インストール

インストール途中で次のようなエラーメッセージが表示されます。

```bash
$ bundle
...
Using tectonic 0.1.0 from https://github.com/xtaniguchimasaya/tectonic-ruby (at master@89d70a4)
tectonic 0.1.0 built to pkg/tectonic-0.1.0.gem.
{}
cargo rustc --release -- -C link-args=-Wl,-undefined,dynamic_lookup
   Compiling proc-macro2 v0.4.24
   Compiling percent-encoding v1.0.1
   Compiling native-tls v0.2.2
   Compiling typeable v0.1.2
   Compiling vec_map v0.8.1
   Compiling antidote v1.0.0
   Compiling semver-parser v0.7.0
   Compiling lazy_static v1.2.0
error: `<core::cell::Cell<T>>::new` is not yet stable as a const fn
  --> /Users/noguchihiroki/.cargo/registry/src/github.com-1ecc6299db9ec823/lazy_static-1.2.0/src/inline_lazy.rs:20:33
   |
20 |     pub const INIT: Self = Lazy(Cell::new(None), ONCE_INIT);
   |                                 ^^^^^^^^^^^^^^^
   |
   = help: in Nightly builds, add `#![feature(const_cell_new)]` to the crate attributes to enable

error: aborting due to previous error

error: Could not compile `lazy_static`.
warning: build failed, waiting for other jobs to finish...
error: build failed
post_install hook at
/Users/noguchihiroki/.rbenv/versions/2.5.0/lib/ruby/gems/2.5.0/bundler/gems/tectonic-ruby-89d70a46d0b6/lib/rubygems_plugin.rb:2
failed for tectonic-0.1.0

In Gemfile:
  tectonic
```