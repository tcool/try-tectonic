require 'sinatra'
require 'tectonic'

latex = <<-EOS
\usepackage{article}
\begin{document}
Hello, Tectonic!
\end{document}
EOS

pdf = Tectonic.latex_to_pdf(latex)

get '/' do
  'hello'
end
